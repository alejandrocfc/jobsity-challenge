const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    resolve: {extensions: ['.js','.jsx']},
    entry: {
        index: "./src/index.js",
        app: "./src/components/App.js",
        days: "./src/components/days.jsx",
        month: "./src/components/month.jsx",
        week: "./src/components/week.jsx"
    },
    output: {
        path: path.join(__dirname, "/dist"),
        filename: '[name].bundle.js'
    },
    optimization: {
        splitChunks: {chunks: 'all'}
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: [/node_modules/],
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        historyApiFallback: true,
        port: 9000
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: './index.html'
        })
    ]
};

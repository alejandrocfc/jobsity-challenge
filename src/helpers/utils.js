import axios from "axios"

export function uniqueID(){
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}

export function getWeather(id) {
    return axios.get(`https://api.openweathermap.org/data/2.5/weather?id=${id}&units=metric&appid=238e78ccdd78ca5804d8edb12cb8eced`)
        .then(({data})=> {
            return `${data.main.temp}°-${data.weather[0].main}`;
        })
        .catch(function (error) {
            return(error);
        });
}

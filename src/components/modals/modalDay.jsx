import React, { useContext } from "react";
import {GlobalContext} from "../../context/global";
import {
    editActivity,
    handleModalActivity,
    handleModalDay,
    removeActivity,
    removeAllActivity
} from "../../context/globalActions";
import moment from "moment";
import MainModal from "./index";

function ModalDay(){
    const [{modalDay,date,byDay},dispatch] = useContext(GlobalContext);
    const showActivity = (info) => {
        const data = {uuid:info.uuid,name:info.name,city:info.city,color:info.color,date:moment(`${info.date},${info.hour}`, "DD-MM-YYYY,HH:mm").toDate()};
        dispatch(editActivity(data));
        dispatch(handleModalActivity(true))
    };
    const deleteActivity = (id) => {
        dispatch(removeActivity(id));
        dispatch(handleModalDay(false));
    };
    return (
        <MainModal modal={modalDay}>
            <div className="title">
                <p>{moment(date, "DD-MM-YYYY").format("MMMM DD, YYYY")}</p>
                <svg id={"close"} onClick={()=>dispatch(handleModalDay(false))}>
                    <path d='M19,3H16.3H7.7H5A2,2 0 0,0 3,5V7.7V16.4V19A2,2 0 0,0 5,21H7.7H16.4H19A2,2 0 0,0 21,19V16.3V7.7V5A2,2 0 0,0 19,3M15.6,17L12,13.4L8.4,17L7,15.6L10.6,12L7,8.4L8.4,7L12,10.6L15.6,7L17,8.4L13.4,12L17,15.6L15.6,17Z' />
                </svg>
            </div>
            <div className="body by__day">
                {
                    byDay.map(day=><CardActivity content={day} showActivity={showActivity} deleteActivity={deleteActivity}/>)
                }
                {byDay.length>0?
                    <button className="remove" onClick={()=>dispatch(removeAllActivity(date))}>X Remove all</button>:
                    <p>Not activities for this day</p>
                }
            </div>
        </MainModal>
    )
}

const CardActivity = (props) => {
    const {name,city,color,weather,uuid} = props.content;
    return (
        <div className="card" key={uuid}>
            <div className="card__color" style={{backgroundColor:color?color:""}}/>
            <div className="card__content">
                <h2 className="card__title">{name}</h2>
                <p className="card__info">{city.label}, {weather}</p>
            </div>
            <div className="card__action-bar">
                <div className="card__button edit" onClick={()=>props.showActivity(props.content)}>Edit</div>
                <div className="card__button" onClick={()=>props.deleteActivity(uuid)}>Remove</div>
            </div>
        </div>
    )
}

export default ModalDay

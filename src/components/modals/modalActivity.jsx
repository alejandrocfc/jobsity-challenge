import React, { useState, useContext } from "react";
import AsyncSelect from "react-select/async";
import DatePicker from "react-datepicker";
import { ChromePicker } from "react-color";
import {GlobalContext} from "../../context/global";
import {addActivity, handleModalActivity, removeActivity, updateActivity} from "../../context/globalActions";
import Cities from "../../helpers/city.json";
import moment from "moment";
import {getWeather, uniqueID} from "../../helpers/utils";
import MainModal from "./index";

function ModalActivity() {
    const [{modalActivity,info},dispatch] = useContext(GlobalContext);
    const [name, setName] = useState(info.name?info.name:"");
    const [city, setCity] = useState(info.city?info.city:"");
    const [color, setColor] = useState(info.color?info.color:"");
    const [showColor, setShowColor] = useState(false);
    const [date, setDate] = useState(info.date?info.date:new Date());
    const saveActivity = async() => {
        const payload = {name,color,city,date:moment(date).format("DD-MM-YYYY"),hour:moment(date).format("HH:mm")};
        const weather = city.value ? await getWeather(city.value) : "";
        if(info.uuid) dispatch(updateActivity(Object.assign(payload,{uuid:info.uuid,weather})));
        else dispatch(addActivity(Object.assign(payload,{uuid:uniqueID(),weather})));
        dispatch(handleModalActivity(false))
    };
    const deleteActivity = () => {
        dispatch(removeActivity(info.uuid));
        dispatch(handleModalActivity(false))
    };
    const fetchCities = input =>
        new Promise(resolve => {
            if(input.length > 3 || city.value) {
                const value = input ? input : city.label;
                resolve(Cities.filter(i=>i.name.toLowerCase().includes(value.toLowerCase())).map(c=>({label:c.name,value:c.id})));
            }
            else resolve([])
        });
    return (
        <MainModal modal={modalActivity}>
            <div className="title">
                <svg id={"close"} onClick={()=>dispatch(handleModalActivity(false))}>
                    <path d='M19,3H16.3H7.7H5A2,2 0 0,0 3,5V7.7V16.4V19A2,2 0 0,0 5,21H7.7H16.4H19A2,2 0 0,0 21,19V16.3V7.7V5A2,2 0 0,0 19,3M15.6,17L12,13.4L8.4,17L7,15.6L10.6,12L7,8.4L8.4,7L12,10.6L15.6,7L17,8.4L13.4,12L17,15.6L15.6,17Z' />
                </svg>
            </div>
            <div className="body">
                <label>Name</label>
                <input type='text' maxLength="30" placeholder="Max. 30 characters" autoFocus={true} value={name} onChange={e=>setName(e.target.value)}/>
                <label>City</label>
                <AsyncSelect className="select" onChange={(value)=>setCity(value)} value={city}
                             cacheOptions defaultOptions loadOptions={fetchCities}/>
                <label>Date</label>
                <DatePicker selected={date} onChange={date => setDate(date)} showTimeSelect
                            timeFormat="HH:mm" timeIntervals={15} timeCaption="time" dateFormat="MMM d, yyyy h:mm aa"/>
                <div className="color">
                    <label>Color</label>
                    <div id={"color"} style={{backgroundColor:`${color}`}} onClick={()=>setShowColor(true)} />
                    {showColor&&<div className="picker">
                        <ChromePicker color={color} onChangeComplete={(color)=>setColor(color.hex)}/>
                        <span onClick={()=>setShowColor(false)}>Elegir</span>
                    </div>}
                </div>
                <button onClick={saveActivity} disabled={!(city.value&&name.length>0&&date)}>Save</button>
                {info.uuid&&<button className="remove" onClick={deleteActivity}>Delete</button>}
            </div>
        </MainModal>
    )
}

export default ModalActivity

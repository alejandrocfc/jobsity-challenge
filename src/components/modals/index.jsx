import React from "react";
import Modal from "react-modal";

export default function MainModal({...props}) {
    return (
        <Modal isOpen={props.modal} className="modal">
            {props.children}
        </Modal>
    )
}

Modal.setAppElement("#root");

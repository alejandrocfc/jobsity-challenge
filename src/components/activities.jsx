import React from "react";
import {editActivity, handleModalActivity, handleModalDay, setByDays, setDate} from "../context/globalActions";
import moment from "moment";

function Activities(props) {
    const {info,activities,dispatch} = props;
    const onClick = (info) => {
        const data = {uuid:info.uuid,name:info.name,city:info.city,color:info.color,date:moment(`${info.date},${info.hour}`, "DD-MM-YYYY,HH:mm").toDate()};
        dispatch(editActivity(data));
        dispatch(handleModalActivity(true))
    };
    const list = activities.filter(a=>a.date===info.date).sort((a,b)=> moment(a.hour,"HH:mm").format("HHmm") - moment(b.hour,"HH:mm").format("HHmm"));
    const openDay = () => {
        dispatch(setByDays(list));
        dispatch(setDate(info.date));
        dispatch(handleModalDay(true))
    };
    return (
        <div>
            <span onClick={openDay}>{info.number}</span>
            {list.slice(0,3).map(i=>
                <div key={i.uuid} className="activity" style={i.color?{borderLeft:`0.5rem solid ${i.color}`}:{}} onClick={()=>onClick(i)}>
                    <div className="activity-content">
                        <div className="name">{i.name}</div>
                        {i.weather&&<div className="weather">{i.weather}</div>}
                    </div>
            </div>)}
            {list.length>3&&<p>{list.length-3}+</p>}
        </div>
    )
}

export default Activities

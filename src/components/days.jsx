import React, {useState,useEffect,useContext} from "react";
import moment from "moment";
import {GlobalContext} from "../context/global";
import Activities from "./activities";

function Days(props) {
    const {display} = props;
    const month = moment(display).format("YYYY-M");
    const [content, setContent] = useState([]);
    const [prev, setPrev] = useState([]);
    const [post, setPost] = useState([]);
    useEffect(()=>{
        //Get number of days of the month
        const nDays = moment(month,"YYYY-M").daysInMonth();
        //Get number of days of the prev month
        const prevDays = moment(display).subtract(1, 'months').daysInMonth();
        //Get position of the day where month start
        const iDay = moment(month,"YYYY-M").day();
        //Create array with days of previous month and display number. Use reverse to change the position
        const init = Array.apply(null, Array(iDay)).map(function (x, i) { return prevDays-i; }).reverse();
        setPrev(init);
        //Create array with days of next month and display number.
        const end = Array.apply(null, Array(35-(iDay+nDays))).map(function (x, i) { return i+1; });
        setPost(end);
        //Create array
        const content = Array.apply(null, Array(nDays)).map(function (x, i) {
            const number = i+1;
            return {number,date:moment(`${display.year()}-${display.month()+1}-${number}`,"YYYY-MM-D").format("DD-MM-YYYY")}; });
        setContent(content);
    },[display]);
    const [{ activities }, dispatch] = useContext(GlobalContext);
    return (
        <div className="days">
            {prev.map((k,i)=><div className="day is-grey" key={i}>{k}</div>)}
            {content.map((k,i)=><div className="day" key={i}>
                <Activities info={k} activities={activities} dispatch={dispatch}/>
            </div>)}
            {post.map((k,i)=><div className="day is-grey" key={i}>{k}</div>)}
        </div>
    )
}

export default Days

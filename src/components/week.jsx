import React, {useContext} from "react";
import {GlobalContext} from "../context/global";
import {addActivity} from "../context/globalActions";

function Week(props) {
    const [{}, dispatch] = useContext(GlobalContext);
    return (
        <div className="week">
            <div onClick={()=>dispatch(addActivity({ads:1}))}>Sunday</div>
            <div>Monday</div>
            <div>Tuesday</div>
            <div>Wednesday</div>
            <div>Thursday</div>
            <div>Friday</div>
            <div>Saturday</div>
        </div>
    )
}

export default Week

import React, { useState, useReducer } from 'react';
import Moment from "moment";
import { GlobalContext, globalState, globalReducer} from "../context/global";
import Month from "./month";
import Week from "./week";
import Days from "./days";
import ModalActivity from "./modals/modalActivity";
import ModalDay from "./modals/modalDay";
import {handleModalActivity} from "../context/globalActions";

function App(){
    const [month, setMonth] = useState(Moment());
    const [state, dispatch] = useReducer(globalReducer, globalState);
    return (
        <main>
            <GlobalContext.Provider value={[state,dispatch]}>
                <Month display={month} setDisplay={setMonth}/>
                <div className="content">
                    <Week/>
                    <Days display={month}/>
                </div>
                <div className="float-btn">
                    <button className="action-btn" onClick={()=>dispatch(handleModalActivity(true))}/>
                </div>
                {state.modalActivity&&<ModalActivity/>}
                {state.modalDay&&<ModalDay/>}
            </GlobalContext.Provider>
        </main>
    )
}

export default App;

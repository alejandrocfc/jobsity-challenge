import React, { useState} from "react";
import MainModal from "./modals";
import moment from "moment";

function Month(props) {
    const {display,setDisplay} = props;
    const [modal,setModal] = useState(false);
    const [month,setMonth] = useState(moment(display).format("YYYY-MM"));
    const saveMonth = () => {
        setDisplay(moment(month,"YYYY-MM"));
        setModal(false)
    };
    return (
        <div className="month">
            <time>
                {moment(display).format("MMMM")}
                <em onClick={()=>setModal(true)}>{moment(display).format("YYYY")}</em>
            </time>
            {
                modal && <MainModal modal={modal} className="modal">
                    <div className="title">
                        <svg id={"close"} onClick={()=>setModal(false)}>
                            <path d='M19,3H16.3H7.7H5A2,2 0 0,0 3,5V7.7V16.4V19A2,2 0 0,0 5,21H7.7H16.4H19A2,2 0 0,0 21,19V16.3V7.7V5A2,2 0 0,0 19,3M15.6,17L12,13.4L8.4,17L7,15.6L10.6,12L7,8.4L8.4,7L12,10.6L15.6,7L17,8.4L13.4,12L17,15.6L15.6,17Z' />
                        </svg>
                    </div>
                    <div className="body">
                        <label htmlFor="month">Select Month</label>
                        <input type="month" name="month" value={month} onChange={e=>setMonth(e.target.value)}/>
                        <button onClick={saveMonth}>Save</button>
                    </div>
                </MainModal>
            }
        </div>
    )
}

export default Month

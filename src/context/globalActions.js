export const actionTypes = {
    ADD_ACTIVITY: 'ADD_ACTIVITY',
    EDIT_ACTIVITY: 'EDIT_ACTIVITY',
    UPDATE_ACTIVITY: 'UPDATE_ACTIVITY',
    REMOVE_ACTIVITY: 'REMOVE_ACTIVITY',
    REMOVE_ALL_ACTIVITY: 'REMOVE_ALL_ACTIVITY',
    DELETE_DAY_ACTIVITIES: 'DELETE_DAY_ACTIVITIES',
    MODAL_ACTIVITY: 'MODAL_ACTIVITY',
    MODAL_DAY: 'MODAL_DAY',
    ACTIVITIES_BY_DAY: 'ACTIVITIES_BY_DAY',
    SET_DATE: 'SET_DATE'
};

export const addActivity = info => ({
    type: actionTypes.ADD_ACTIVITY,
    info
});

export const editActivity = (info) => ({
    type: actionTypes.EDIT_ACTIVITY,
    payload:info
});

export const updateActivity = info => ({
    type: actionTypes.UPDATE_ACTIVITY,
    info
});

export const removeActivity = id => ({
    type: actionTypes.REMOVE_ACTIVITY,
    payload:id
});

export const removeAllActivity = date => ({
    type: actionTypes.REMOVE_ALL_ACTIVITY,
    payload:date
});

export const handleModalActivity = (modal) => ({
    type: actionTypes.MODAL_ACTIVITY,
    payload:modal
});

export const handleModalDay = (modal) => ({
    type: actionTypes.MODAL_DAY,
    payload:modal
});

export const setByDays = (list) => ({
    type: actionTypes.ACTIVITIES_BY_DAY,
    payload:list
});

export const setDate = (date) => ({
    type: actionTypes.SET_DATE,
    payload:date
});

import { createContext } from 'react';
import { actionTypes } from './globalActions';

export const GlobalContext = createContext(null);

export const globalState = {
    info: {},
    activities: [],
    date:"",
    byDay: [],
    modalDay:false,
    modalActivity:false
};

export const globalReducer = (state, action) => {
    Object.freeze(state);
    switch (action.type) {
        case actionTypes.ADD_ACTIVITY:
            const info = action.info;
            return {
                ...state,
                activities:[...state.activities,info]
            };
        case actionTypes.EDIT_ACTIVITY:
            return {
                ...state,
                info: action.payload,
            };
        case actionTypes.UPDATE_ACTIVITY:
            const list = state.activities.map(i=> i.uuid === action.info.uuid ? Object.assign(i,action.info) : i);
            return {...state,activities:list};
        case actionTypes.REMOVE_ACTIVITY:
            return {...state, activities:state.activities.filter(i=> i.uuid !== action.payload)};
        case actionTypes.REMOVE_ALL_ACTIVITY:
            return {...state, activities:state.activities.filter(i=> i.date !== action.payload),byDay:[]};
        case actionTypes.DELETE_DAY_ACTIVITIES:
            const componentName = action.componentName;
            return {
                ...state,
                componentName,
            };
        case actionTypes.MODAL_ACTIVITY:
            return {
                ...state,
                info:action.payload?state.info:{},
                modalActivity:action.payload,
            };
        case actionTypes.MODAL_DAY:
            return {
                ...state,
                byDay:action.payload?state.byDay:[],
                modalDay:action.payload,
            };
        case actionTypes.ACTIVITIES_BY_DAY:
            return {
                ...state,
                byDay:action.payload,
            };
        case actionTypes.SET_DATE:
            return {
                ...state,
                date:action.payload,
            };
        default:
            return state;
    }
};

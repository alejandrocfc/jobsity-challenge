## Jobsity Challenge

### Autor: Germán Alejandro Mora Gómez 

## Requirements
- Node.js >= 9.x.x or Yarn >= 1.x.x

## Installation
- Clone repo.
```
$ git clone https://gitlab.com/alejandrocfc/jobsity-challenge
```
- Go into the project folder.
- Install dependencies
```
$ npm install || yarn install
```
## Comments
Use React Hooks and Context to handle global state. The styles are pure using flex to show days. 
Use different libraries to wrap UI as a React-select and react-date-picker. 
Moment.js was used to parse date

## Development Environment
To deploy the project run command:
```
$ npm run start
```
## Production Environment
The project is deploy on Netlify [https://jobsity-challenge.netlify.com/](https://jobsity-challenge.netlify.com/)
